﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Shop.Models
{
    public class MvcAffableBeanContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public MvcAffableBeanContext() : base("name=MvcAffableBeanContext")
        {
        }

        public System.Data.Entity.DbSet<MvcAffableBean.Models.Category> Categories { get; set; }

        public System.Data.Entity.DbSet<MvcAffableBean.Models.Cart> Carts { get; set; }

        public System.Data.Entity.DbSet<MvcAffableBean.Models.Product> Products { get; set; }

        public System.Data.Entity.DbSet<MvcAffableBean.Models.ShoppingCart> ShoppingCarts { get; set; }
    }
}
